#!/usr/bin/env python
# -*- coding: utf-8 -*-
import traceback
from modules import Movement, Controller, Coilgun, Video, Interface
from time import sleep, time
from helpers import autodetect_ports

def main():
    print "Running app"
    
    #TEMPORARY CONF VALUES
    ROT_SPEED = 35
    FOW_SPEED = 75
    SIDE_SPEED = 40
    ROT_SPEED_GATE = 100
    ROT_SPEED_GATE_AIM = 10
    BACK_UP_FROM_GATE_SPEED = 30 #NEEDS TWEAKING

    GATE_MAX_SIZE = 400

    counter = 0
                
    states = {'searching_for_ball': 0,
              'moving_to_ball': 1,
              'searching_for_gate': 2,
              'aiming_for_gate': 3}
    
    ports = autodetect_ports()
    
    video = Video()
    gate_functions = [video.get_blue_gate_position_from_center,
                      video.get_yellow_gate_position_from_center]
    gate_nr = 0
    state = 0

    
    controller = Controller(0)
    interface = Interface()
    movement = Movement(False, ports[:3])
    coilgun = Coilgun(ports[-1])
    coilgun.autocharge_on()
    sTime = time()

    integral = 0
    ball_Kp = 1
    ball_Ki = 0
    
    try:
        while True:
            print state
            mode = interface.getMode()
            if (mode == 0):
                gate_nr = interface.getGate()
                state = -1
            elif (mode == 1):
                direction = controller.get_left_direction()
                velocity = 100 * controller.get_left_depth()
                rotate = -50 * controller.get_axis('RIGHTRL')
                movement.move_with_rotation(velocity, direction, rotate)
                #ball = video.get_biggest_ball_position_from_center()
                gate = gate_functions[gate_nr](False)
                #print gate
                #print ball
        
                if (controller.get_button('START')):
                    break
                
                if (controller.get_button('A')):
                    coilgun.kick(8)
        
                    sleep(0.01)
            elif (mode == 2):
                if state < 0:
                    state = 0
            else:
                break
            coilgun.ping()
            coilgun.charge()
            if state == states['searching_for_ball']:
                ball = video.get_biggest_ball_position_from_center()
                if (ball):
                    movement.move_with_rotation(0, 0, 0)
                    state = states['moving_to_ball']
                    continue
                else:
                    movement.move_with_rotation(0, 0, -ROT_SPEED * 0.7)
            elif state == states['moving_to_ball']:
                ball = video.get_biggest_ball_position_from_center()
                if (not ball):
                    state = states['searching_for_ball']
                    continue
                elif (movement.get_ball()):
                    state = states['searching_for_gate']
                else:
                    error = ball[0]
                    integral = integral + error * 0.03
                    output = ball_Kp * error + ball_Ki * integral
                    output = -ROT_SPEED * output
                    
                    if abs(error < 0.5):
                        speed_p = (0.9 - ball[1] if ball[1] > 0 else 1) * FOW_SPEED
                        speed_p = speed_p if speed_p >= 15 else 15
                        movement.move_with_rotation(speed_p, 0, output)
                    else:
                        movement.move_with_rotation(0, 0, output)
            elif state == states['searching_for_gate']:
                if (not movement.get_ball()):
                    state = states['searching_for_ball']
                    continue
                else:
                    gate = gate_functions[gate_nr](False)
                    if (gate):
                        movement.move_with_rotation(0, 0, 0)
                        state = states['aiming_for_gate']
                        continue
                    else:
                        movement.rotate_around_ball(ROT_SPEED_GATE)
                        
            elif state == states['aiming_for_gate']:
                gate = gate_functions[gate_nr](True)
                ball = movement.get_ball()
                if (not gate):
                    state = states['searching_for_gate']
                elif (not ball):
                    state = states['searching_for_ball']
                else:
                    gate_from_center = gate[0]
                    print gate[5]
                    if (gate[5]):
                        dir = 270 if gate[0] > 0 else 90
                        movement.move(SIDE_SPEED, dir)
                        continue
                    if (gate[4]):
                        if (counter >= 5):
                            coilgun.kick(8)
                            counter = 0
                        else:
                            counter += 1
                            movement.rotate_around_ball(-ROT_SPEED_GATE * gate_from_center * 0.5)
                    else:
                        if (gate[2] > GATE_MAX_SIZE): #NEEDS TWEAKING
                            movement.move_with_rotation(BACK_UP_FROM_GATE_SPEED, 180, -ROT_SPEED_GATE * gate_from_center)
                        else:
                            movement.rotate_around_ball(-ROT_SPEED_GATE * gate_from_center)
            curTime = time()
            time_elapsed = (curTime - sTime)
            sTime = curTime
            #print round(1 / time_elapsed, 0)
        movement.move(0, 0)
        coilgun.discharge()
        sleep(0.2)
        video.__del__()
        movement.__del__()
        coilgun.__del__()
        interface.__del__()
        #controller.__del__()
    except Exception:
        traceback.print_exc()
        movement.move(0, 0)
        coilgun.discharge()
        sleep(0.2)
        video.__del__()
        movement.__del__()
        coilgun.__del__()
        interface.__del__()
        #controller.__del__()

if __name__ == '__main__':
    main()
