import serial

def autodetect_ports():
    ports_found = 0
    ports = [None, None, None, None]
    
    for i in range(255):
        if (ports_found == 4):
            break;
        try:
            ser = serial.Serial(port='COM' + str(i), baudrate=115200)
            #Write '?' to ask id
            ser.write('?\n')
            #Receive and parse id
            read = ser.read(7)
            if read == "dischar":
                ser.read(4)
                read = ser.read(7)
            wheel_id = int(read.split(':')[1][:-2])
            if wheel_id not in [1, 2, 3, 4]:
                raise Exception('Exception', 'Wheel id(s) incorrect!')
            #Assign connections to variables assuming wheels are identified FRONT-LEFT 1, FRONT-RIGHT 2, REAR 3
            if (wheel_id == 1):
                ports[0] = 'COM' + str(i)
            elif (wheel_id == 2):
                ports[1] = 'COM' + str(i)
            elif (wheel_id == 3):
                ports[2] = 'COM' + str(i)
            else:
                ports[3] = 'COM' + str(i)
            ports_found += 1
            ser.close()
        except Exception:
            pass
    if None in ports:
        return 1
    else:
        return ports

autodetect_ports()
