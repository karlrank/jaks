#!/usr/bin/env python
# -*- coding: utf-8 -*-

import serial
from math import cos, radians

COM_PORTS = ['COM4', 'COM5', 'COM6']
wheel_fr = None
wheel_fl = None
wheel_r = None

for port in COM_PORTS:
    ser = serial.Serial(port=port, baudrate=115200)
    ser.write('?\n')
    wheel_id = int(ser.read(7).split(':')[1][:-2])
    if (wheel_id == 1):
        wheel_fl = ser
    elif (wheel_id == 2):
        wheel_fr = ser
    else:
        wheel_r = ser

velocity = 50
desiredDirection = 0

fA = velocity*(cos(radians(150-desiredDirection)))
print radians(150-desiredDirection)
       
fB = velocity*(cos(radians(30-desiredDirection)))
print radians(30-desiredDirection)

fC = velocity*(cos(radians(270-desiredDirection)))
print radians(270-desiredDirection)

wheel_fl.write('sd' + str(round(fA)) +'\n')
wheel_fr.write('sd' + str(round(fB)) +'\n')
wheel_r.write('sd' + str(round(fC)) +'\n')

"""
from config import COM_PORTS
from modules import Movement
move = Movement(False, COM_PORTS)
"""
