# Sample Python/Pygame Programs
# Simpson College Computer Science
# http://cs.simpson.edu
 
import pygame
import serial
from math import cos, radians, atan, degrees, sqrt

COM_PORTS = ['COM3', 'COM7', 'COM8']
wheel_fr = None
wheel_fl = None
wheel_r = None
BASE_VELOCITY = 100

for port in COM_PORTS:
    ser = serial.Serial(port=port, baudrate=115200)
    ser.write('?\n')
    wheel_id = int(ser.read(7).split(':')[1][:-2])
    if (wheel_id == 1):
        wheel_fl = ser
    elif (wheel_id == 2):
        wheel_fr = ser
    else:
        wheel_r = ser
 
# Define some colors
black    = (   0,   0,   0)
white    = ( 255, 255, 255)
blue     = (  50,  50, 255)
green    = (   0, 255,   0)
dkgreen  = (   0, 100,   0)
red      = ( 255,   0,   0)
purple   = (0xBF,0x0F,0xB5)
brown    = (0x55,0x33,0x00)
 
# Function to draw the background
def draw_background(screen):
    # Set the screen background
    screen.fill(white)
 
def draw_item(screen,x,y):
    pygame.draw.rect(screen,green,[0+x,0+y,30,10],0)
    pygame.draw.circle(screen,black,[15+x,5+y],7,0)
     
pygame.init()
 
        
screen = pygame.display.set_mode((100, 100))
 
# Current position
x_coord=10
y_coord=10
 
# Count the joysticks the computer has
joystick_count=pygame.joystick.get_count()
if joystick_count == 0:
    # No joysticks!
    print ("Error, I didn't find any joysticks.")
else:
    # Use joystick #0 and initialize it
    my_joystick = pygame.joystick.Joystick(0)
    my_joystick.init()
             
clock = pygame.time.Clock()
 
done=False
while done==False:
 
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done=True
    
    if pygame.key.get_pressed()[pygame.K_ESCAPE]:
        wheel_fl.write('sd' + str(round(0)) +'\n')
        wheel_fr.write('sd' + str(round(0)) +'\n')
        wheel_r.write('sd' + str(round(0)) +'\n')
        wheel_fl.close()
        wheel_fr.close()
        wheel_r.close()
        break
             
    # As long as there is a joystick
    if joystick_count != 0:
     
        # This gets the position of the axis on the game controller
        # It returns a number between -1.0 and +1.0
        left_rl_axis_pos = my_joystick.get_axis(0)
        left_ud_axis_pos = -1 * my_joystick.get_axis(1)
        trigger_axis_pos = -1 * my_joystick.get_axis(2)
        right_ud_axis_pos = -1 * my_joystick.get_axis(3)
        right_lr_axis_pos = my_joystick.get_axis(4)
        
        direction = 0
        velocity = 0
        rotation = 0
        if (left_rl_axis_pos < 0 and left_ud_axis_pos > 0):
            direction = 90 + degrees(atan(left_ud_axis_pos/left_rl_axis_pos))
        elif (left_rl_axis_pos < 0 and left_ud_axis_pos < 0):
            direction = 90 + degrees(atan(left_ud_axis_pos/left_rl_axis_pos))
        elif (left_rl_axis_pos > 0 and left_ud_axis_pos < 0):
            direction = 270 + degrees(atan(left_ud_axis_pos/left_rl_axis_pos))
        elif (left_rl_axis_pos > 0 and left_ud_axis_pos > 0):
            direction = 270 + degrees(atan(left_ud_axis_pos/left_rl_axis_pos))
        
        velocity = sqrt(left_rl_axis_pos**2 + left_ud_axis_pos**2) * BASE_VELOCITY + trigger_axis_pos * BASE_VELOCITY
        
        rotation = BASE_VELOCITY * right_lr_axis_pos * -1
        
        #print round(left_rl_axis_pos, 2), round(left_ud_axis_pos, 2), round(trigger_axis_pos, 2), round(right_ud_axis_pos, 2), round(right_lr_axis_pos, 2)        

        fA = velocity*(cos(radians(150-direction))) + rotation
        fB = velocity*(cos(radians(30-direction))) + rotation
        fC = velocity*(cos(radians(270-direction))) + rotation

        wheel_fl.write('sd' + str(round(fA)) +'\n')
        wheel_fr.write('sd' + str(round(fB)) +'\n')
        wheel_r.write('sd' + str(round(fC)) +'\n')
    clock.tick(40)
     
pygame.quit ()
