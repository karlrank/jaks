import cv2
import numpy as np


# create video capture
cap = cv2.VideoCapture(0)
cap.set(3, 1366)
cap.set(4, 720)
while True:
    
# read the frames
    _,frame = cap.read()
    cv2.imshow('thresh',frame)
    if cv2.waitKey(5)== 27:
        break

# Clean up everything before leaving
cv2.destroyAllWindows()
cap.release()

