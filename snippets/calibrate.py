import cv2
import numpy as np
from config import COM_PORTS
from modules import Movement
import time

# create video capture
cap = cv2.VideoCapture(0)
cap.set(3, 1280)
cap.set(4, 720)
cap.set(6, 60)
width, height = cap.get(3), cap.get(4)

print width, height

move = Movement(False, COM_PORTS)
tresh_low = np.array((0,0,0), dtype=np.float32)
tresh_high = np.array((255,255,255), dtype = np.float32)

while(1):

    # read the frames
    _,frame = cap.read()


    # convert to hsv and find range of colors
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    thresh = cv2.inRange(hsv, tresh_low, tresh_high)
    kernel = np.ones((8,8), 'float32')
    dil = cv2.dilate(thresh, kernel)
    dil2 = cv2.dilate(thresh,kernel)
    cont = cv2.findContours(dil, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    #print "###", cv2.boundingRect(cont[0]) 
    contours = cont[0]
    objects = []
    for contour in contours:
        if len(contour) > 20:
            cordX = 0
            cordY = 0
            for i in contour:
                cordX += i[0][0]
                cordY += i[0][1]
            cordX /= len(contour)
            cordY /= len(contour)
            objects.append((cordX, cordY, len(contour)))
    
    
    max = objects[0] if len(objects) else (-1,-1,0)
    for i in objects:
        if i[2] > max[2]:
            max = i
    
    #print max
    if max[0] > width / 2:
        #move.rotate_clockwise(15)
        
        print "LEFT"
    else :
        print "RIGHT"
        #move.rotate_counter_clockwise(15)
    
    cv2.imshow('thresh',dil2)
    if cv2.waitKey(10)== 27:
        break

    if cv2.waitKey(10)== 27:
        break
    key = cv2.waitKey(10)
    if key== ord('q'):
        tresh_low[0] +=1
    if key== ord('w'):
        tresh_low[1] +=1
    if key== ord('e'):
        tresh_low[2] +=1
    if key== ord('r'):
        tresh_high[0] +=1
    if key== ord('t'):
        tresh_high[1] +=1
    if key== ord('y'):
        tresh_high[2] +=1
    if key== ord('a'):
        tresh_low[0] -=1
    if key== ord('s'):
        tresh_low[1] -=1
    if key== ord('d'):
        tresh_low[2] -=1
    if key== ord('f'):
        tresh_high[0] -=1
    if key== ord('g'):
        tresh_high[1] -=1
    if key== ord('h'):
        tresh_high[2] -=1
    print tresh_high, tresh_low
    #time.sleep(0.5)

# Clean up everything before leaving
cv2.destroyAllWindows()
cap.release()
