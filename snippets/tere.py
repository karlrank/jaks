import cv2
import numpy as np

# create video capture
cap = cv2.VideoCapture(0)

while(1):

    # read the frames
    _,frame = cap.read()


    # convert to hsv and find range of colors
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    thresh = cv2.inRange(hsv,np.array((5,50,50)), np.array((15,255,255)))

    cv2.imshow('thresh',thresh)
    if cv2.waitKey(5)== 27:
        break

# Clean up everything before leaving
cv2.destroyAllWindows()
cap.release()