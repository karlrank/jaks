import cv2.cv as cv

color_tracker_window = "V2rvi tundmine"

class ColorTracker:
	def __init__(self):
		cv.NamedWindow(color_tracker_window, 1)
		self.capture= cv.CaptureFromCAM(1)

	def run(self):
		while True:
			img = cv.QueryFrame(self.capture)
			hsv_img = cv.CreateImage(cv.GetSize(img), 8, 3)
			cv.CvtColor(img, hsv_img, cv.CV_BGR2HSV)
			thresholded_img =  cv.CreateImage(cv.GetSize(hsv_img), 8, 1)
			cv.InRangeS(hsv_img, (5, 50, 50), (15, 255, 255), thresholded_img)
			mat = cv.GetMat(thresholded_img)
			moments = cv.Moments(mat, 0)
			area = cv.GetCentralMoment(moments, 0, 0)
			if(area>100000):
				x = cv.GetSpatialMoment(moments, 1, 0) / area
				y = cv.GetSpatialMoment(moments, 0, 1) / area
				center = (int(x), int(y))
				overlay = cv.CreateImage(cv.GetSize(img), 8, 3)
				cv.Circle(overlay, center, 2, (255, 255, 255), 20)
				cv.Add(img, overlay, img)
				#cv.Merge(mat, None, None, None, img)
			cv.ShowImage(color_tracker_window, img)
			if cv.WaitKey(10) == 27:
				break

if __name__ == "__main__":
	color_tracker = ColorTracker()
	color_tracker.run()
				
