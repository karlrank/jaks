import cv2
import numpy as np
from config import COM_PORTS
from modules import Movement
import time

# create video capture
cap = cv2.VideoCapture(1)
cap.set(3, 640)
cap.set(4, 480)
cap.set(cv2.cv.CV_CAP_PROP_FPS, 30)
width, height = cap.get(3), cap.get(4)

print width, height

move = Movement(False, COM_PORTS)
tresh_low = np.array((10, 111, 117), np.uint8)
tresh_high = np.array((20, 255, 253), np.uint8)
middle = width / 2
searching = 1
while(1):

    # read the frames
    _,frame = cap.read()


    # convert to hsv and find range of colors
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    thresh = cv2.inRange(hsv, tresh_low, tresh_high)
    kernel = np.ones((8,8), 'float32')
    dil = cv2.dilate(thresh, kernel)
    dil2 = cv2.dilate(thresh,kernel)
    cont = cv2.findContours(dil, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    #print "###", cv2.boundingRect(cont[0]) 
    contours = cont[0]
    objects = []
    for contour in contours:
        if len(contour) > 4:
            cordX = 0
            cordY = 0
            for i in contour:
                cordX += i[0][0]
                cordY += i[0][1]
            cordX /= len(contour)
            cordY /= len(contour)
            objects.append((cordX, cordY, len(contour)))
    
    
    max = objects[0] if len(objects) else (-1,-1,0)
    for i in objects:
        if i[2] > max[2]:
            max = i


    
    if (max[0] == -1):
        searching = 2
        move.move_with_rotation(0, 0, 35)
    else:
        print "N2EN PALLI"
        if searching:
            speed = -25 * (searching / 2)
            move.move_with_rotation(0, 0, -35)
            searching -= 1
        else:
            difference = (middle-max[0]) / (width / 2)
            if (difference < 0.5) :
                move.move_with_rotation(50 * 1 - difference, 0, 35 * difference)
            else:            
                move.move_with_rotation(0, 0, 35 * difference)

    
    cv2.imshow('thresh',dil2)
    if cv2.waitKey(5)== 27:
        break
    
    #time.sleep(0.5)

# Clean up everything before leaving
move.move(0,0)
time.sleep(1)
move.__del__()
cv2.destroyAllWindows()
cap.release()
