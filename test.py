from helpers import autodetect_ports
from modules import Controller, Movement, Coilgun
from time import sleep

def run_test():
    print "Running tests..."
    
    ports = autodetect_ports()
    controller = Controller(0)
    movement = Movement(False, ports[:3])
    coilgun = Coilgun(ports[-1])
    coilgun.autocharge_on()
    
    while True:
        print movement.get_ball()
        coilgun.ping()
        coilgun.charge()
        direction = controller.get_left_direction()
        velocity = 150 * controller.get_left_depth()
        rotate = -100 * controller.get_axis('RIGHTRL')
        movement.move_with_rotation(velocity, direction, rotate)
        
        if (controller.get_button('START')):
            break
        
        if (controller.get_button('A')):
            coilgun.kick(10)
        
        sleep(0.01)
    
    movement.move(0, 0)
    movement.__del__()
    coilgun.__del__()

if __name__ == '__main__':
    run_test()
