from threading import Thread
from Tkinter import *
    

class Interface(Tk):
    mode = None
    gate = None
    def __init__(self):
        Tk.__init__(self)
        self.my_widgets()
        self.title('HI IM ROBOT')
        self.minsize(240, 60)
        thread = Thread(target = self.mainloop, args = [])
        thread.start()

    def getMode(self):
        return self.mode.get() if self.mode else 0

    def getGate(self):
        return self.gate.get() if self.gate else 0

    def __del__(self):
        self.destroy()

    def my_widgets(self):
        self.mode = IntVar()
        self.gate = IntVar()
        R1 = Radiobutton(self, text="Initializing", variable=self.mode, value=0)
        R1.grid( row=0, column=1, sticky=W, columnspan=2 )

        R2 = Radiobutton(self, text="Controller", variable=self.mode, value=1)
        R2.grid( row=1, column=1, sticky=W, columnspan=2 )

        R3 = Radiobutton(self, text="Running", variable=self.mode, value=2)
        R3.grid( row=2, column=1, sticky=W, columnspan=2 )

        R4 = Radiobutton(self, text="ABORT!", variable=self.mode, value=3)
        R4.grid( row=3, column=1, sticky=W, columnspan=2 )

        Bg = Radiobutton(self, text="Blue Gate", variable=self.gate, value=0)
        Bg.grid( row=0,  column=4, sticky=W )

        Yg = Radiobutton(self, text="Yellow Gate", variable=self.gate, value=1)
        Yg.grid(row=1, column=4, sticky=W)



