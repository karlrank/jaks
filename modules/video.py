import cPickle as pickle
import cv2
import numpy as np


class Video():
    COLORSETTINGS = None
    
    BALL_SIMILARITY_THRESHOLD = 5
    GATE_MAX_HEIGHT = 160
    GATE_MIN_Y = 10
    LINE_SIZE = 10

    GATE_MIN_WIDTH = 50
    
    ball_thresh_low = None
    ball_thresh_high = None
    ball_erode = 1
    ball_dilate = 6
    ball_size_limit = 15
    
    bluegate_thresh_low = None
    bluegate_thresh_high = None
    bluegate_erode = 1
    bluegate_dilate = 1
    bluegate_size_limit = 0
    
    yellowgate_thresh_low = None
    yellowgate_thresh_high = None
    yellowgate_erode = 1
    yellowgate_dilate = 1
    yellowgate_size_limit = 100
    
    borderline_thresh_low = None
    borderline_thresh_high = None
    borderline_erode = 1
    borderline_dilate = 1
    borderline_size_limit = 100
    
    capture = None
    width = 0
    height = 0

    dribbler_point = (320.0, 440.0)
      
    def __init__(self, camera=1, configfile='config.pkl'):
        self.load_pickled_settings(configfile)
        self.setup_tresholds()
        
        #Set up capture
        self.capture = cv2.VideoCapture(camera)
        self.capture.set(3, 640)
        self.capture.set(4, 480)
        self.capture.set(cv2.cv.CV_CAP_PROP_FPS, 30)
        self.width, self.height = self.capture.get(3), self.capture.get(4)
        
        pass
    
    def __del__(self):
        cv2.destroyAllWindows()
        self.capture.release()
    
    def load_pickled_settings(self, configfile):
        pkl_file = open(configfile, 'rb')
        self.COLORSETTINGS = pickle.load(pkl_file)['colorsettings']
        print self.COLORSETTINGS
        pkl_file.close()
    
    def setup_tresholds(self):
        self.ball_thresh_low = np.array(self.COLORSETTINGS['ball'][0], np.uint8)
        self.ball_thresh_high = np.array(self.COLORSETTINGS['ball'][1], np.uint8)
        self.ball_erode = int(self.COLORSETTINGS['ball'][2])
        self.ball_dilate = int(self.COLORSETTINGS['ball'][3])
        self.ball_size_limit = int(self.COLORSETTINGS['ball'][4])
        
        self.bluegate_thresh_low = np.array(self.COLORSETTINGS['bluegate'][0], np.uint8)
        self.bluegate_thresh_high = np.array(self.COLORSETTINGS['bluegate'][1], np.uint8)
        self.bluegate_tresh_erode = np.array(self.COLORSETTINGS['bluegate'][2], np.uint8)
        self.bluegate_tresh_dilate = np.array(self.COLORSETTINGS['bluegate'][3], np.uint8)
        self.bluegate_size_limit = np.array(self.COLORSETTINGS['bluegate'][4], np.uint8)
        
        self.yellowgate_thresh_low = np.array(self.COLORSETTINGS['yellowgate'][0], np.uint8)
        self.yellowgate_thresh_high = np.array(self.COLORSETTINGS['yellowgate'][1], np.uint8)
        self.yellowgate_erode = int(self.COLORSETTINGS['yellowgate'][2])
        self.yellowgate_dilate = int(self.COLORSETTINGS['yellowgate'][3])
        self.yellowgate_size_limit = int(self.COLORSETTINGS['yellowgate'][4])
        
        self.borderline_thresh_low = np.array(self.COLORSETTINGS['borderline'][0], np.uint8)
        self.borderline_thresh_high = np.array(self.COLORSETTINGS['borderline'][1], np.uint8)
        self.borderline_erode = int(self.COLORSETTINGS['borderline'][2])
        self.borderline_dilate = int(self.COLORSETTINGS['borderline'][3])
        self.borderline_size_limit = int(self.COLORSETTINGS['borderline'][4])
    
    def get_height(self, object):
        return object[1]

    def get_width(self, object):
        return object[7]
    
    def get_contour_x(self, object):
        return object[0][0]
    
    def get_contour_y(self, object):
        return object[0][1]
    
    def get_borderline_thresh(self, frame, thresh_low, thresh_high, dilate):
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        thresh = cv2.inRange(hsv, thresh_low, thresh_high)
        kernel = np.ones((dilate, dilate), 'uint8')
        dil = cv2.dilate(thresh, kernel)
        return dil
        
    
    def capture_transform_to_objects(self, frame, thresh_low, thresh_high, dilate):
        #Convert to hsv and find range of colors
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        thresh = cv2.inRange(hsv, thresh_low, thresh_high)
        kernel = np.ones((dilate, dilate), 'uint8')
        dil = cv2.dilate(thresh, kernel)
        return dil
    
    def get_objects_from_contour(self, image, object_size_limit):
        contours = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        objects = []
        for contour in contours[0]:
            if len(contour) > object_size_limit:
                #Calculate middle point of object
                cordX = 0
                cordY = 0
                for i in contour:
                    cordX += i[0][0]
                    cordY += i[0][1]
                cordX /= len(contour)
                cordY /= len(contour)
                #Calculate width of object
                x_sorted_contour = sorted(contour, key=self.get_contour_x)
                y_sorted_contour = sorted(contour, key=self.get_contour_y)
                x_min = x_sorted_contour[0][0][0]
                x_max = x_sorted_contour[-1][0][0]
                y_min = y_sorted_contour[0][0][1]
                y_max = y_sorted_contour[-1][0][1]
                
                objects.append((cordX, cordY, len(contour), x_max, x_min, y_max, y_min, x_max - x_min, y_max - y_min))
        
        return sorted(objects, key=self.get_height)

    def get_line_between_ball(self, image, ball):
        ballx = ball[0]
        bally = ball[1]

        m = (ballx - self.dribbler_point[0]) / (bally - self.dribbler_point[1])
        points = []
        for i in range(bally, int(self.dribbler_point[1])):
            x = int(m * (i - self.dribbler_point[1]) + self.dribbler_point[0])
            points.append(image[i][x])
        if (points):
            counter = 0
            temp_counter = 0
            previous_i = points[0]
            for i in points:
                if (i and previous_i):
                    temp_counter += 1
                if (temp_counter > counter):
                    counter = temp_counter
                previous_i = i

            return True if counter > self.LINE_SIZE else False
        else:
            return False
    
    def get_biggest_ball_position_from_center(self):
        _, frame = self.capture.read()
        contours = self.capture_transform_to_objects(frame, self.ball_thresh_low, self.ball_thresh_high, self.ball_dilate)
        balls = self.get_objects_from_contour(contours, self.ball_size_limit)
        border_thresh = self.get_borderline_thresh(frame, self.borderline_thresh_low, self.borderline_thresh_high, self.borderline_dilate)
        balls.reverse()
        
        if balls:
            new_balls = []
            for ball in balls:
                if not self.get_line_between_ball(border_thresh, balls[0]):
                    new_balls.append(ball)
                if (len(new_balls) >= 1):
                    break
            balls = new_balls

##            for key, ball in enumerate(balls):
##                if (key == 0):
##                    cv2.circle(frame, (ball[0], ball[1]), 2, (0, 0, 255), 5)
##                else:
##                    cv2.circle(frame, (ball[0], ball[1]), 2, (255, 0, 0), 5)
##         cv2.imshow('Ballz', frame)
##            if cv2.waitKey(5)== 27:
##                pass

            if not balls:
                return False
            ball = balls[0]
            
            x = (ball[0] - self.width / 2) / (self.width / 2)
            y = (ball[1] - self.height / 2) / (self.height / 2)
            return (x, y, ball[7], ball[8])
        else:
            return False
    
    def get_yellow_gate_position_from_center(self, aiming):
        _, frame = self.capture.read()
        contours = self.capture_transform_to_objects(frame, self.yellowgate_thresh_low, self.yellowgate_thresh_high, self.yellowgate_dilate)
        gate = self.get_objects_from_contour(contours, self.yellowgate_size_limit)
        gate = sorted(gate, key=self.get_width)
        gate = gate[-1] if gate else False

        ball_in_way = False
        if aiming:
            contours_ball = self.capture_transform_to_objects(frame, self.ball_thresh_low, self.ball_thresh_high, self.ball_dilate)
            balls = self.get_objects_from_contour(contours_ball, self.ball_size_limit)
            for b in balls:
                if b[0] > 310 and b[0] < 330:
                    ball_in_way = True
                    break
        
        
        if gate and gate[7] > self.GATE_MIN_WIDTH:
##            cv2.circle(frame, (gate[0], gate[1]), 2, (0, 255, 0), 5)
##            cv2.imshow('Ballz', frame)
##            if cv2.waitKey(5)== 27:
##                pass
            
            x = (gate[0] - self.width / 2) / (self.width / 2)
            y = (gate[1] - self.height / 2) / (self.height / 2)
            if (gate[3] > 330 and gate[4] < 310):
                kick = True
            else:
                kick = False
            return (x, y, gate[7], gate[8], kick, ball_in_way)
        else:
            return False
    
    def get_blue_gate_position_from_center(self, aiming):
        _, frame = self.capture.read()
        contours = self.capture_transform_to_objects(frame, self.bluegate_thresh_low, self.bluegate_thresh_high, self.bluegate_dilate)
        gate = self.get_objects_from_contour(contours, self.bluegate_size_limit)
        gate = sorted(gate, key=self.get_width)
        gate = gate[-1] if gate else False

        ball_in_way = False
        if aiming:
            contours_ball = self.capture_transform_to_objects(frame, self.ball_thresh_low, self.ball_thresh_high, self.ball_dilate)
            balls = self.get_objects_from_contour(contours_ball, self.ball_size_limit)
            for b in balls:
                if b[0] > 290 and b[0] < 350 and b[1] < 400:
                    ball_in_way = True
                    break
        
        if gate and gate[7] > self.GATE_MIN_WIDTH:
##            cv2.circle(frame, (gate[0], gate[1]), 2, (0, 255, 0), 5)
##            cv2.imshow('Ballz', frame)
##            if cv2.waitKey(5)== 27:
##                pass
            x = (gate[0] - self.width / 2) / (self.width / 2)
            y = (gate[1] - self.height / 2) / (self.height / 2)
            if (gate[3] > 340 and gate[4] < 300):
                kick = True
            else:
                kick = False
            return (x, y, gate[7], gate[8], kick, ball_in_way)
        else:
            return False
        
        
    

























    
