from helpers import autodetect_ports
from math import cos, radians
import time
import serial

class Movement():
    ports = None
    
    #Wheels' serial objects
    ser_wheel_fl = None
    ser_wheel_fr = None
    ser_wheel_r = None
    time_from_last_command = time.time()
    
    #True if ready to move, False if any errors occur
    movement_ok = True
    
    def __init__(self, autodetect, ports):
        if autodetect:
            self.ports = autodetect_ports()
        else:
            self.ports = ports
        
        if (self.ports and len(self.ports) == 3):
            self.init_wheels();
        else:
            self.movement_ok = False
    
    def __del__(self):
        #Close serial connections to wheels when exiting.
        try:
            self.ser_wheel_fl.close();
            self.ser_wheel_fr.close();
            self.ser_wheel_r.close();
        except(Exception):
            self.movement_ok = False
            print "Error closing wheel connections!"
    
    def init_wheels(self):
        try:
            for port in self.ports:
                #Open serial connection
                ser = serial.Serial(port=port, baudrate=115200)
                #Write '?' to ask id
                ser.write('?\n')
                #Receive and parse id
                wheel_id = int(ser.read(7).split(':')[1][:-2])
                if wheel_id not in [1, 2, 3]:
                    raise Exception('Exception', 'Wheel id(s) incorrect!')
                #Assign connections to variables assuming wheels are identified FRONT-LEFT 1, FRONT-RIGHT 2, REAR 3
                if (wheel_id == 1):
                    self.ser_wheel_fl = ser
                elif (wheel_id == 2):
                    self.ser_wheel_fr = ser
                else:
                    self.ser_wheel_r = ser
        except (Exception):
            print "Error initializing wheels"
            self.movement_ok = False
    
    def write_pid(self, speeds):
        #now = time.time()
        #print "Time since last commad", now - self.time_from_last_command
        #self.time_from_last_command = now
        if (len(speeds) == 3):
            try:
                self.ser_wheel_fl.write('sd' + str(speeds[0]) +'\n')
                self.ser_wheel_fr.write('sd' + str(speeds[1]) +'\n')
                self.ser_wheel_r.write('sd' + str(speeds[2]) +'\n')
            except Exception:
                self.movement_ok = False
                return 1
            return 0
        else:
            return 1
    
    def calculate_speeds(self, velocity, direction, rotational_velocity):
            fA = round(velocity*(cos(radians(150-direction))) + rotational_velocity)
            fB = round(velocity*(cos(radians(30-direction))) + rotational_velocity)
            fC = round(velocity*(cos(radians(270-direction))) + rotational_velocity)
            
            return [fA, fB, fC]
    
    def move(self, velocity, direction):
        if (direction >= 0 and direction <= 360):
            if self.write_pid(self.calculate_speeds(velocity, direction, 0)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def move_with_rotation(self, velocity, direction, rotational_velocity):
        if (direction >= 0 and direction <= 360):
            if self.write_pid(self.calculate_speeds(velocity, direction, rotational_velocity)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def move_forward(self, velocity):
        if (velocity):
            if self.write_pid(self.calculate_speeds(velocity, 0, 0)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def move_backward(self, velocity):
        if (velocity):
            if self.write_pid(self.calculate_speeds(velocity, 180, 0)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def move_back(self, velocity):
        if (velocity):
            if self.write_pid(self.calculate_speeds(velocity, 180, 0)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def move_left(self, velocity):
        if (velocity):
            if self.write_pid(self.calculate_speeds(velocity, 90, 0)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def move_right(self, velocity):
        if (velocity):
            if self.write_pid(self.calculate_speeds(velocity, 270, 0)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def rotate_clockwise(self, rotational_velocity):
        if (rotational_velocity):
            if self.write_pid(self.calculate_speeds(0, 0, -1 * rotational_velocity)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def rotate_counter_clockwise(self, rotational_velocity):
        if (rotational_velocity):
            if self.write_pid(self.calculate_speeds(0, 0, rotational_velocity)) == 0:
                return 0
            else:
                return 1
        else:
            return 1
    
    def get_ball(self):
        self.ser_wheel_fl.read(self.ser_wheel_fl.inWaiting())
        self.ser_wheel_fl.write("gb\n")
        ball = int(self.ser_wheel_fl.read(6).strip()[3:4])
        return True if ball else False

    def rotate_around_ball(self, speed):
        self.write_pid([speed * 0.2, speed * 0.4, speed])
        
        
        
