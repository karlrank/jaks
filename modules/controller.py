import pygame
from threading import Thread
from math import degrees, atan, sqrt

#Controller configuration
MAP_AXIS = ['LEFTRL', 'LEFTUD', 'TRIGGER', 'RIGHTUD', 'RIGHTRL']
MAP_BUTTONS = ['A','B','X','Y','LB','RB','SELECT','START','L','R']
MAP_HATS = ['DPAD']
DEADZONE = 0.15

class Controller():
    controller_ok = True
    
    controller = None
    
    num_axis = 0
    num_buttons = 0
    num_hats = 0
    
    axis_map = None
    button_map = None
    hat_map = None
    
    axis = None
    buttons = None
    hats = None
    
    deadzone = 0
    
    thread = None
    
    
    def __init__(self, controller_id=0, axis=MAP_AXIS, buttons=MAP_BUTTONS, hats=MAP_HATS, dead=DEADZONE):
        pygame.init()
        
        if pygame.joystick.get_count() == 0:
            self.controller_ok = False
            return
        
        try:
            self.controller = pygame.joystick.Joystick(0)
            self.controller.init()
        except (Exception):
            print "Error initializing controller."
            self.controller_ok = False
            return
        
        self.num_axis = len(axis)
        self.num_buttons = len(buttons)
        self.num_hats = len(hats)
        self.deadzone = dead
        
        self.axis_map = axis
        self.button_map = buttons
        self.hat_map = hats
        
        self.axis = dict(zip(axis, [0 for i in range(self.num_axis)]))
        self.buttons = dict(zip(buttons, [0 for i in range(self.num_buttons)]))
        self.hats = dict(zip(hats, [0 for i in range(self.num_hats)]))
        
        try:
            self.thread = Thread(target = self.run, args = [])
            self.thread.start()
        except(Exception):
            print "Error initializing thread."
            self.controller_ok = False
            return
    
    
    def __del__(self):
        self.controller.quit()
        pygame.quit()
    
    
    def run(self):
        clock = pygame.time.Clock()
        
        while True:
            
            for event in pygame.event.get():
                pass
            
            for key, name in enumerate(self.axis_map):
                value = self.controller.get_axis(key)
                self.axis[name] = value if (value > self.deadzone or value < -self.deadzone) else 0
            for key, name in enumerate(self.button_map):
                self.buttons[name] = self.controller.get_button(key)
            for key, name in enumerate(self.hat_map):
                self.hats[name] = self.controller.get_hat(key)
                        
            clock.tick(50)
    
    def get_left_direction(self):
        rl = self.axis['LEFTRL']
        ud = -self.axis['LEFTUD']
        
        direction = 0
        if (rl < 0 and ud > 0):
            direction = 90 + degrees(atan(ud/rl))
        elif (rl < 0 and ud < 0):
            direction = 90 + degrees(atan(ud/rl))
        elif (rl > 0 and ud < 0):
            direction = 270 + degrees(atan(ud/rl))
        elif (rl > 0 and ud > 0):
            direction = 270 + degrees(atan(ud/rl))

        if (rl == 0 and ud < 0):
            direction = 180
        elif (ud == 0 and rl > 0):
            direction = 270
        elif (ud == 0 and rl < 0):
            direction = 90
    
        return direction
    
    def get_right_direction(self):
        rl = self.axis['RIGHTRL']
        ud = -self.axis['RIGHTUD']
        
        direction = 0
        if (rl < 0 and ud > 0):
            direction = 90 + degrees(atan(ud/rl))
        elif (rl < 0 and ud < 0):
            direction = 90 + degrees(atan(ud/rl))
        elif (rl > 0 and ud < 0):
            direction = 270 + degrees(atan(ud/rl))
        elif (rl > 0 and ud > 0):
            direction = 270 + degrees(atan(ud/rl))
    
        return direction
    
    def get_left_depth(self):
        rl = self.axis['LEFTRL']
        ud = -self.axis['LEFTUD']
        
        depth = sqrt(rl**2 + ud**2)
        return depth if depth < 1 else 1
    
    def get_right_depth(self):
        rl = self.axis['RIGHTRL']
        ud = -self.axis['RIGHTUD']
        
        depth = sqrt(rl**2 + ud**2)
        return depth if depth < 1 else 1
    
    def get_trigger_depth(self):
        return -self.axis['TRIGGER']
    
    def get_axis(self, axis):
        return self.axis[axis]
    
    def get_button(self, button):
        return self.buttons[button]
    
    def get_hat(self, hat):
        return self.hats[hat]
    
    
        
        
        
        
        
        
        
