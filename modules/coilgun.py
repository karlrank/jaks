import serial
from time import time

class Coilgun():
    coilgun = None
    last_kick_time = None
    
    def __init__(self, port):
        if port:
            self.coilgun = serial.Serial(port=port, baudrate=115200)
            self.last_kick_time = time()
            
    def __del__(self):
        self.coilgun.close()
    
    def ping(self):
        self.coilgun.write('p\n')
    
    def charge(self):
        self.coilgun.write('c\n')
    
    def discharge(self):
        self.coilgun.write('c\n')
        
    def autocharge_on(self):
        self.coilgun.write('ac1\n')
    
    def autocharge_off(self):
        self.coilgun.write('ac0\n')
    
    def kick(self, power):
        current = time()
        if current - self.last_kick_time < 1:
            return
        else:
            self.last_kick_time = current
            power_string = 3200 * power
            self.coilgun.write('k' + str(power_string) + '\n')
